const inquirer = require('inquirer');
const chalk = require('chalk');
const path = require('path');

const validateFilename = (value) => {
    if (!value.endsWith('.csv')) {
        return 'File name must ends with csv';
    }
    if (value.split('.')[0] === '') {
        return 'File name can\'t be empty';
    }
    return true;
}


const questions = [
    {
        type: 'input',
        name: 'cardName',
        message: chalk.blueBright.bold("Please enter a card name (i.e Ron-High-tech-Zone): "),
        validate: (cardName) => !!cardName
    },
    {
        type: 'input',
        name: 'newExpenses',
        message: chalk.blueBright.bold("Please enter 'Cal' Expenses csv file name (in current directory): "),
        validate: validateFilename
    },
    {
        type: 'input',
        name: 'oldExpenses',
        message: chalk.blueBright.bold("Please enter an existing 'Expense Manager' csv file name (Optional): "),
        validate: (value) => value ? validateFilename(value) : true
    }
];

const getInputs = async () => {
    const { newExpenses, oldExpenses, cardName } = await inquirer.prompt(questions);
    return {
        newExpensesPath: path.resolve(__dirname, newExpenses),
        oldExpensesPath: oldExpenses ? path.resolve(__dirname, oldExpenses) : null,
        cardName
    };
};


module.exports = getInputs;