const fs = require('fs');
const doesFileExist = async (path) => {
    try {
        await fs.promises.access(path);
        return true;
    } catch (error) {
        return false;
    }
}
module.exports = { doesFileExist }