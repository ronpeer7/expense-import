const { parse: csvParse, } = require('fast-csv');
const { writeToPath } = require('@fast-csv/format');
const { doesFileExist } = require('./file-util');
const fs = require('fs');

const read = (path) => {
    return new Promise(async (resolve, reject) => {
        if (!await doesFileExist(path)) {
            return resolve(null);
        }
        const results = [];
        const options = { headers: true, ignoreEmpty: true };
        fs.createReadStream(path)
            .pipe(csvParse(options))
            .on('data', (data) => results.push(data))
            .on('error', reject)
            .on('end', () => { resolve(results) })

    });
};

const write = (path, data) => {
    return new Promise((resolve, reject) => {
        const options = { headers: true, includeEndRowDelimiter: true, rowDelimiter: ',\n', writeBOM: true };
        writeToPath(path, data, options)
            .on('error', reject)
            .on('finish', resolve);
    });
};
module.exports = { read, write };