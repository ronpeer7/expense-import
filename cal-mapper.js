const moment = require('moment');
const CAL_CONSTANTS = require('./constants/cal-constants.json');
const EXPENSE = require('./constants/default-expense.json');
const map = (cardName, csvExpenses, initialRawId) => csvExpenses.map((expense, index) => ({
    ...EXPENSE,
    "Date": getDate(expense),
    "Amount": getAmount(expense),
    "Payment Method": cardName,
    "Description": getDescription(expense),
    "Row Id": getRawId(initialRawId, index),
})
);

const getDate = (expense) => moment(expense[CAL_CONSTANTS.EXPENSE_TIME], 'DD/MM/YY').format('YYYY-MM-DD');
const getAmount = (expense) => -Number(expense[CAL_CONSTANTS.AMOUNT].replace(/₪|,/g, ''));
const getDescription = (expense) => expense[CAL_CONSTANTS.NAME];
const getRawId = (initialRawId, index) => initialRawId + index;

module.exports = map;
