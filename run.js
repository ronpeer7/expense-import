const csvUtil = require('./utils/csv-util');
const map = require('./cal-mapper');
const getInputs = require('./userPrompt');
const chalk = require('chalk');
const figlet = require('figlet');
const moment = require('moment');

const showTitle = () => {
    console.log(
        chalk.yellow.bold(
            figlet.textSync('Expense Import', { horizontalLayout: 'default' })
        )
    );
    console.log('\n');
}

const handleError = (error) => {
    if (error.isTtyError) {
        console.log(chalk.red('Your environment doesn\'t support this tool!'));
    } else {
        console.log(chalk.red('Error occurred: ', error.message));
    }
};

const getLastRawId = (expenses) => (expenses && expenses.length > 0)
    ? Number(expenses[expenses.length - 1]['Row Id'])
    : 0;

const run = async () => {
    try {
        showTitle();
        const { newExpensesPath, oldExpensesPath, cardName } = await getInputs();
        const newExpenses = await csvUtil.read(newExpensesPath);
        if (!newExpenses) {
            console.log(chalk.red.bold(`New expenses havn't been provided!`));
            return;
        }
        const oldExpenses = await csvUtil.read(oldExpensesPath);
        const initialRawId = getLastRawId(oldExpenses) + 1;
        const newExpensesMapped = map(cardName, newExpenses, initialRawId);
        const totalExpenses = oldExpenses ? oldExpenses.concat(newExpensesMapped) : newExpensesMapped;
        const totalExpensesPath = `Total-Expenses-${moment().format('YYYY-MM-DDThh_mm_ss')}.csv`;
        await csvUtil.write(totalExpensesPath, totalExpenses);
        console.log(chalk.green.bold(`Expense import ended! Check ${totalExpensesPath}`));
    } catch (error) {
        handleError(error);
    }
};

run();

